﻿using System;
namespace UserAccountNamespace
{
    public abstract class UserAccount
    {
        private string full_name;
        protected string user_name;
        protected string user_password;
        public UserAccount()
        {
        }
        public UserAccount(string name, string uName, string password)
        {
            this.full_name = name;
            this.user_name = uName;
            this.user_password = password;
        }
        //return this.user_name.Equals(uName) && this.user_password.Equals(password) ? true : false;
        public abstract bool validateLogin(string uName, string password);
        public string getFullName
        {
            get { return this.full_name; }
            set { this.full_name = value; }
        }

    }
}
