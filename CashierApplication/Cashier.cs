﻿using System;
namespace UserAccountNamespace
{
    public class Cashier : UserAccount
    {
        private string department { get; set; }
        public Cashier()
        {
        }
        public Cashier(string name, string department, string uName, string password)
        {
            getFullName = name;
            this.department = department;
            user_name = uName;
            user_password = password;
        }

        public override bool validateLogin(string uName, string password)
        {
            return user_name.Equals(uName) && user_password.Equals(password) ? true : false;
        }
        public string getDepartment
        {
            get { return this.department; }
            set { this.department = value; }
        }
    }
}
