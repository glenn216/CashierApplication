﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserAccountNamespace;
namespace CashierApplication
{
    public partial class frmLoginAccount : Form
    {
        public frmLoginAccount()
        {
            InitializeComponent();
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            string fullname = "Glenn S. Alon";
            string username = tbUsername.Text;
            string password = tbPassword.Text;
            string department = "Information and Communication Technologies Department";
            Cashier cashier = new Cashier(fullname, department, username, password);
            switch (cashier.validateLogin("glenn216", "1234"))
            {
                case true:
                    {
                        MessageBox.Show($"Welcome {cashier.getFullName} of {cashier.getDepartment}.");
                        frmPurchaseDiscount frmPurchaseDiscount = new frmPurchaseDiscount();
                        frmPurchaseDiscount.Show();
                        this.Hide();
                        break;
                    }

                default:
                    MessageBox.Show("Your username and password does not match.", "Login Failed!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }
    }
}
